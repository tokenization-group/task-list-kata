package action

import repository.ITaskRepository
import java.lang.Exception

class AddDeadline (
        private val taskRepository: ITaskRepository
) {

    fun execute(taskId: Int, dueDate: String) {
        val task = taskRepository.find(taskId) ?: throw Exception()
        task.updateDueDate(dueDate)
        taskRepository.save(task)
    }

}