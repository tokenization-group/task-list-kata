package repository

import domain.Task

class TaskRepository : ITaskRepository {

    private val tasks = arrayListOf<Task>()

    override fun add(task: Task) {
        tasks.add(task)
    }

    override fun find(taskId: Int): Task? {
        return tasks.find { task -> task.taskId() == taskId }
    }

    override fun save(task: Task) {
        tasks[tasks.indexOf(find(task.taskId()))] = task
    }

}