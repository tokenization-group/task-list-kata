package repository

import domain.Task

interface ITaskRepository {

    fun add(task: Task)
    fun find(taskId: Int): Task?
    fun save(task: Task)

}