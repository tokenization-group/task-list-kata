package domain

class Task (
        private val taskId: Int,
        private var dueDate: String? = null
) {

    fun taskId(): Int {
        return taskId
    }

    fun dueDate(): String? {
        return dueDate
    }

    fun updateDueDate(dueDate: String) {
        this.dueDate = dueDate
    }

}