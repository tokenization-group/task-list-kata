import action.AddDeadline
import domain.Task

import org.junit.Before
import org.junit.Test

import org.junit.Assert

import repository.ITaskRepository
import repository.TaskRepository

class DeadlineTest {

    private val dueDate = "17/10/2020"
    private val taskId = 1

    private lateinit var addDeadline : AddDeadline
    private lateinit var taskRepository: ITaskRepository

    @Before
    fun setup() {
        taskRepository = TaskRepository()
        addDeadline = AddDeadline(taskRepository)
    }

    @Test
    fun `task deadline added successfully`() {
        given_a_pending_task()
        when_a_deadline_is_added_to_the_task()
        then_the_task_has_a_due_date()
    }

    private fun given_a_pending_task() {
        taskRepository.add(Task(taskId))
    }

    private fun when_a_deadline_is_added_to_the_task() {
        addDeadline.execute(taskId, dueDate);
    }

    private fun then_the_task_has_a_due_date() {
        val task = taskRepository.find(taskId)
        Assert.assertEquals(dueDate, task?.dueDate())
    }

}