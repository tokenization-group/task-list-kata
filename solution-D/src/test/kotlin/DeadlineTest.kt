import org.junit.Assert
import org.junit.Test
import java.util.*

class DeadlineTest {
    lateinit var retrieveTasks: RetrieveTasks
    lateinit var tasksRepository: TaskRepository
    var tasks =  ArrayList<Any>()
    val todayDate = ""


    @Test
    public fun search_task_dealine_until_today(){
        given_some_tasks_with_optional_deadline()
        when_retrieving_tasks_until_today()
        then_verify_tasks_are_retrieved()
    }

    private fun then_verify_tasks_are_retrieved() {
        verify_tasks_quantity()
        verify_due_date_of_tasks()
    }

    private fun verify_due_date_of_tasks() {
    }

    private fun verify_tasks_quantity() {
        assert(tasks.size == tasksRepository.findByDueDate(todayDate).size)
    }

    private fun given_some_tasks_with_optional_deadline() {
        tasksRepository = InMemoryTaskRepository()
        tasksRepository.insert(Task("1","2020-10-04"))
        tasksRepository.insert(Task("2","2055-10-04"))
        tasksRepository.insert(Task("3","2020-10-01"))
        retrieveTasks = RetrieveTasks(tasksRepository)
    }

    private fun when_retrieving_tasks_until_today() {
        tasks = retrieveTasks.execute(todayDate);
    }

}