interface TaskRepository {
    abstract fun insert(task: Task)
    abstract fun findByDueDate(dueDate: String): Collection<Task>

}
