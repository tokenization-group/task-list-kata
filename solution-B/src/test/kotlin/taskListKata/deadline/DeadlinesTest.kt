package taskListKata.deadline

import org.junit.Test
import taskListKata.deadline.actions.SetDeadlineCommand
import taskListKata.deadline.infrastructure.TaskRepository
import taskListKata.deadline.model.Task

class DeadlinesTest {

    lateinit var taskId: String
    lateinit var deadlineDate: String
    lateinit var deadlineId: String
    val taskRepository = TaskRepository()
    val setDeadlineCommand = SetDeadlineCommand(taskRepository)

    @Test
    fun give_a_task_a_deadline() {
        given_a_task()
        when_we_receive_a_command_with_id_and_a_date(taskId, deadlineId, deadlineDate);
        then_each_task_receive_a_command();
    }

    private fun given_a_task() {
        taskId = "112233"
        deadlineDate = "16/10/2020"
        deadlineId = "334455"
        taskRepository.add(taskId, Task(taskId))
    }


    private fun when_we_receive_a_command_with_id_and_a_date(taskId: String, deadlineId: String, date: String) {
        setDeadlineCommand.execute(taskId, deadlineId, date)
    }

    private fun then_each_task_receive_a_command() {
        val modifiedTask: Task = taskRepository.getById(taskId)
        assert(modifiedTask.verify(deadlineDate, deadlineId))
    }
}