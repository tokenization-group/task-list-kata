package taskListKata.deadline.actions

import taskListKata.deadline.infrastructure.TaskRepository
import taskListKata.deadline.model.Task

class SetDeadlineCommand(val taskRepository: TaskRepository) {

    fun execute(taskId: String, deadlineId: String, deadlineDate: String) {
       val task =  taskRepository.getById(taskId)
        task.modify(deadlineId, deadlineDate)
        taskRepository.add(taskId,task)
    }
}
