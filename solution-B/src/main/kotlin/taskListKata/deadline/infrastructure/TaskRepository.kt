package taskListKata.deadline.infrastructure

import taskListKata.deadline.model.Task

class TaskRepository {
    val tasks = HashMap<String,Task>()

    fun getById(taskId: String): Task {
        return tasks.getOrDefault(taskId, Task("","",""))
    }

    fun add(taskId: String, task: Task) {
        tasks.put(taskId,task)
    }
}
