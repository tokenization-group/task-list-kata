package taskListKata.deadline.model

class Task(val taskId: String, private var deadlineId:String?="", private var deadlineDate:String?="") {
    fun modify(deadlineId: String, deadlineDate: String) {
        this.deadlineId = deadlineId
        this.deadlineDate = deadlineDate
    }

    fun verify(deadlineDate: String, deadlineId: String): Boolean {
        return this.deadlineId.equals(deadlineId) && this.deadlineDate.equals(deadlineDate)
    }
}
